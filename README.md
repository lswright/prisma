# README #

PRISMA - Patients at risk of missing appointments.  This was an NHS hackday (Cardiff 28th+29th Jan 2017) pitch and project. 

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
***IMPORTANT: for some reason the links don't work when clicked on this page - please copy and paste into the address bar instead ***
Currently installed on herokou ( [https://prisma-app.herokuapp.com/risk_patients](Link URL) )
This is a ruby on rails application, so if you know your way around that, you should be able to install the app.  Once it's running,simply create a new patient and give them attributes.  Have a look at the brief video here that explains how this works ( [https://youtu.be/pmlDPYwziMY](Link URL)  ).  How to, to follow.
Please look in the dummy data and ipynb folders too.

more info on google drive here [https://drive.google.com/open?id=0Bw0Dbx2EyQT4amd4RkhMZTBjVFE](Link URL)
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Lesa Wright, @DoSho, lswright@gmail.com (repo owner and admin)
Thomas Davey, thomas.davey@gmail.com (SQL - test data)
Matt Peet, mpeet, mpeet99@googlemail.com (data structure)
Hannah Dahwa, (UI, presentation)
Gareth Morlais, @digitalst, melynmelyn@gmail.com (data structure)
Michael George, @drmgeorge87, michael.george.2@gmail.com (Frontend + ruby)
Ella Preston, ellapreston@hotmail.co.uk (data structure)
Ricardo Colasanti, ric.colasanti@gmailcom (Decision tree - python)
Tom Powell, t.powelly48@gmail.com (data structure)
Paul McEnhill, paulmcenhill@gmail.com, paulmcenhill@gmail.com (data structure)
Janet Holoway (design)