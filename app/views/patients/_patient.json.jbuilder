json.extract! patient, :id, :name, :dob, :tel, :distance, :absentee, :created_at, :updated_at
json.url patient_url(patient, format: :json)