class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.date :dob
      t.string :tel
      t.float :distance
      t.float :absentee

      t.timestamps null: false
    end
  end
end
