class AddRiskAndReasonToPatientsTable < ActiveRecord::Migration
  def change
  	add_column :patients, :risk, :boolean
  	add_column :patients, :reason, :string
  end
end
