Rails.application.routes.draw do
  resources :patients
  root to: 'visitors#index'

  get '/risk_patients/', to: 'patients#risk_patients', as: 'risk_patients'
end
